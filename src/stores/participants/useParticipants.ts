/* eslint-disable @typescript-eslint/no-explicit-any */
import { isTokenExpired } from "@/helpers/token-utils";
import { getParticipants } from "@/services/Participants";
import { useAuth } from "@/stores/Auth";
import { create } from "zustand";

export type Filter = {
  company_search: string;
  identify_search: string;
  start_date: string;
  end_date: string;
};

type State = {
  loading: false;
  filters: Filter;
  pagination: { perPage: number; page: number };
  total: number;
  participants: any[];
  //
  overlay: boolean;
};

type Actions = {
  toFetchMonitoring: () => Promise<void>;
  setFilter: (filter: Filter) => void;
  setPagination: (pagination: { perPage: number; page: number }) => void;
  setOverlay: (overlay: boolean) => void;
};

type ExamInfo = {
  exam_date: Date | null;
  replacement_date: Date | null;
  note: string | null;
};

const getExamsInfo = (exams: any): ExamInfo => {
  const firstExam = exams[0];
  const secondExam = exams[1];

  if (!exams || exams.length === 0) {
    return { exam_date: null, replacement_date: null, note: "~" };
  }
  if (exams.length === 1) {
    return {
      exam_date: new Date(firstExam?.created_at) || null,
      replacement_date: null,
      note: firstExam?.note ?? "~",
    };
  }
  if (exams.length >= 2) {
    return {
      exam_date: new Date(firstExam?.created_at) || null,
      replacement_date: new Date(secondExam?.created_at) || null,
      note: secondExam?.note ?? "~",
    };
  }
  return { exam_date: null, replacement_date: null, note: "~" };
};

export const useMonitoring = create<State & Actions>((set) => ({
  loading: false,

  filters: {
    company_search: "",
    identify_search: "",
    start_date: "",
    end_date: "",
  },
  pagination: {
    perPage: 7,
    page: 1,
  },

  total: 0,
  participants: [],

  overlay: false,

  setOverlay(overlay: boolean) {
    set({ overlay });
  },

  setFilter(filters: Filter) {
    set({ filters });
    if (filters.start_date && !filters.end_date) return;
    useMonitoring.getState().toFetchMonitoring();
  },

  setPagination(pagination: { perPage: number; page: number }) {
    set({ pagination });
    useMonitoring.getState().toFetchMonitoring();
  },

  async toFetchMonitoring() {
    const { token } = useAuth.getState();
    if (!token || isTokenExpired(token)) return;

    try {
      const params = {
        ...useMonitoring.getState().filters,
        ...useMonitoring.getState().pagination,
      };
      const { data } = await getParticipants(params);
      set({
        participants: data?.data?.map((participant: any) => {
          const { exam_date, replacement_date, note } = getExamsInfo(
            participant?.exams
          );

          return {
            id: participant?.id || "",
            participant_id: participant?.participant_id || "",
            participant: participant?.participant || null,
            company_search:
              participant?.active_course?.company?.business_name || "",
            identify_search: participant?.participant?.value_document || "",
            full_name: participant?.participant?.full_name || "",
            course: participant?.active_course?.course?.name || "",
            status: participant?.status || "",
            exams: participant?.exams || [],
            exam_date,
            replacement_date,
            note,
            ...participant
          };
        }),
        pagination: {
          page: data?.current_page,
          perPage: data?.per_page,
        },
        total: data?.total,
      });
    } catch (error) {
      console.error("Error fetching participants:", error);
    }
  },
}));
