import { create } from 'zustand';

export const USER_LOCAL_KEY = 'user_key';

type State = {
  isOpened: boolean;
};

type Actions = {
  toggle: () => void;
};

export const useSidebar = create<State & Actions>((set) => ({
  isOpened: false,
  toggle: () => set((state) => ({ isOpened: !state.isOpened }))
}));
