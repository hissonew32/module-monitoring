import axiosClient from "@/axiosClient";
import { AuthLogin, type Credentials, type User } from "@/services/Auth";
import { create } from "zustand";

export const USER_LOCAL_KEY = "USER";
export const TOKEN_LOCAL_KEY = "TOKEN";

type State = {
  email: string;
  password: string;
  user: User | null;
  token: string | null;
  loading?: boolean;
};

type Actions = {
  loggin: (credentials: Credentials) => Promise<State>;
  refreshToken: () => Promise<string>;
  logout: () => Promise<boolean>;
};

const getUser = () => {
  const user = localStorage.getItem(USER_LOCAL_KEY);

  if (!user) return null;

  return JSON.parse(user);
};

const getToken = () => {
  const token = localStorage.getItem(TOKEN_LOCAL_KEY);

  if (!token) return null;

  try {
    return JSON.parse(token);
  } catch (error) {
    return null;
  }
};

export const useAuth = create<State & Actions>((set) => ({
  loading: false,
  // data
  email: "",
  password: "",
  user: getUser(),
  token: getToken(),
  // actions
  loggin: async (credentials: Credentials) => {
    set(() => ({ loading: true }));

    try {
      const data = await AuthLogin({...credentials, host: window.location.hostname});

      if (data && !data.success) {
        set({ email: "", password: "", user: null, token: null });

        return data;
      }

      if (data && data.success) {
        set(() => ({
          ...credentials,
          user: { ...data.user, role: data.role, email: credentials.email },
          token: data.token,
        }));

        localStorage.setItem(
          USER_LOCAL_KEY,
          JSON.stringify({ ...data.user, role: data.role, email: credentials.email })
        );
        localStorage.setItem(TOKEN_LOCAL_KEY, JSON.stringify(data.token));
      }

      return data;
    } catch (error) {
      return error;
    } finally {
      set(() => ({ loading: false }));
    }
  },
  refreshToken: async () => {
    try {
      const { data } = await axiosClient.post("/refresh", {});
      if (data) {
        localStorage.setItem(TOKEN_LOCAL_KEY, JSON.stringify(data.token));
        set({ token: data.token });
      }
      return data;
    } catch (error) {
      console.error(error);
    }
  },
  logout: async () => {
    try {
      const { data } = await axiosClient.post("/logout");
      if (data.success) {
        localStorage.removeItem(USER_LOCAL_KEY);
        localStorage.removeItem(TOKEN_LOCAL_KEY);
        set({ user: null, token: null });
      }
      return data.success;
    } catch (error) {
      console.error(error);
    }
  },
}));
