import { Route, BrowserRouter as Router, Routes } from 'react-router-dom';

import Login from '@/pages/Login';
import Monitoring from '@/pages/Monitoring';

function App() {
  return (
    <Router>
      <Routes>
        <Route path='/' Component={Login} />

        <Route path='/dashboard' Component={Monitoring} />
      </Routes>
    </Router>
  );
}

export default App;
