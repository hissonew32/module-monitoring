import PromotLogo from '@/assets/ProMot-by-HISSO.svg'
import PromotIcono from '@/assets/Promot-icono.svg'

import { useAuth } from '@/stores/Auth'
import { ErrorMessage, Field, Form, Formik } from 'formik'
import { Navigate, useNavigate } from 'react-router-dom'
import { Toaster } from 'sonner'
import * as Yup from 'yup'

const Login = () => {
	const { loggin, loading, token } = useAuth()
	const navigate = useNavigate()

	if (token) {
		return <Navigate to='/dashboard' />
	}

	const LoginSchema = Yup.object().shape({
		email: Yup.string().email('Dirección de correo electrónico no válida').required('Correo es requerido'),
		password: Yup.string()
			// .min(8, 'La contraseña debe tener al menos 8 caracteres')
			.required('Contraseña es requerida')
	})

	return (
		<div
			className='w-full h-screen flex flex-col lg:flex-row gap-6 lg:gap-2 items-center justify-center lg:justify-around'
			style={{
				backgroundImage: `url(/static/soltect-banner.jpg)`,
				backgroundColor: '#1e3b47',
				backgroundRepeat: 'no-repeat',
				backgroundSize: 'cover',
				backgroundPosition: 'center'
			}}
		>
			<div className='text-white flex flex-col gap-4 text-center'>
				<div className='text-4xl lg:text-6xl font-medium'>Bienvenido a</div>
				<img
					className='mt-8 mx-auto w-[200px] h-[100px] lg:w-[300px] lg:h-[150px]'
					src={PromotLogo}
					alt='fondo tecnologico'
				/>
				<div className='text-2xl lg:text-4xl font-medium'>Soluciones que impulsan</div>
			</div>
			<div className='max-w-[450px] w-full text-gray-600 bg-[#f2f5f7] p-[30px] rounded-xl'>
				<div className='text-center'>
					<img src={PromotIcono} width={50} className='mx-auto' />
					<div className='mt-6 space-y-2'>
						<p className='text-gray-800 text-3xl font-bold lg:text-[40px]'>Inicia sesión</p>
						<p className='py-4'>
							{/* ¿No tienes cuenta?{' '}
							<a className='font-medium text-indigo-600 hover:text-indigo-500 cursor-pointer'>Registrate</a> */}
							Por favor, ingrese los datos solicitados
						</p>
					</div>
				</div>
				<Formik
					initialValues={{
						email: '',
						password: ''
					}}
					validationSchema={LoginSchema}
					onSubmit={async (values) => {
						const response = await loggin(values)

						if (response && response.user) {
							navigate('/dashboard')
						}
					}}
				>
					<Form className='mt-4 text-center'>
						<div className='mb-4'>
							<label className='font-medium text-sm' htmlFor='email'>
								Usuario <span className='text-red-400'>*</span>
							</label>
							<Field
								type='email'
								id='email'
								name='email'
								className='w-full mt-2 px-3 py-2 text-gray-500 bg-transparent outline-none border focus:border-indigo-600 shadow-sm rounded-lg text-[14px]'
								placeholder='Ingresar Usuario'
							/>
							<ErrorMessage name='email' component='div' className='text-red-400' />
						</div>

						<div className='mb-4'>
							<label className='font-medium text-sm' htmlFor='password'>
								Contraseña <span className='text-red-400'>*</span>
							</label>
							<Field
								type='password'
								id='password'
								name='password'
								className='w-full mt-2 px-3 py-2 text-gray-500 bg-transparent outline-none border focus:border-indigo-600 shadow-sm rounded-lg text-[14px]'
								placeholder='Contraseña'
							/>
							<ErrorMessage name='password' component='div' className='text-red-400' />
						</div>

						{/* <div className='text-center my-4'>
							<a className='hover:text-sky-300 text-sky-500 hover:underline cursor-pointer'>Olvidé mi contraseña</a>
						</div> */}

            {/* <div className='text-start flex items-center my-4 mb-5'>
              <input className='mr-0 mt-1' type="checkbox" name="remember" id="remember" />
              <label htmlFor="remember" className='ml-2 cursor-pointer block'>Recordarme</label>
            </div> */}

						<button
							className='disabled:bg-indigo-300 mt-8 disabled:cursor-not-allowed px-5 py-2 text-white bg-sky-500 hover:bg-sky-400 active:bg-sky-600 rounded-md duration-150 text-[14px]'
							type='submit'
							disabled={loading}
						>
							INICIAR SESIÓN
						</button>
					</Form>
				</Formik>
			</div>

			<Toaster richColors position='top-center' />
		</div>
	)
}

export default Login
