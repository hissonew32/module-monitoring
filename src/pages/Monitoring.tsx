import Overlay from '@/components/Atoms/Overlay';
import ListMonitoring from '@/components/Molecules/Monitoring/ListMonitoring';
import Dashboard from '@/layout/Dashboard';
import { useMonitoring } from '@/stores/participants/useParticipants';

const Monitoring = () => {
  const { overlay } = useMonitoring()

  return (
    <Dashboard>
      <div className='text-lg uppercase mb-4'>PERSONAL GENERAL</div>
			
			{
				overlay && <Overlay />
			}
			

      {/* <Card>
				<div className='flex items-center gap-2'>
					<SearchInput
						value={filter.dni}
						placeholder='Filtrar por dni...'
						onChange={(e) => setFilter({ ...filter, dni: e.target.value })}
					/>

					<SearchInput
						value={filter.participant}
						placeholder='Filtrar por participante...'
						onChange={(e) =>
							setFilter({ ...filter, participant: e.target.value })
						}
					/>

					<Button icon={DownloadIcon} text='Descargar' />
				</div>
			</Card> */}

      <ListMonitoring />
    </Dashboard>
  );
};

export default Monitoring;
