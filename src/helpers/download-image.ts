import axios from 'axios';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export async function toDownloadImage(url: string, body: any, fileName: string) {
  const { data } = await axios.post(url, { ...body }, { responseType: 'blob' });

  const a = document.createElement('a');
  a.href = URL.createObjectURL(data);
  a.download = fileName;

  document.body.appendChild(a);
  a.click();
  document.body.removeChild(a);
}
