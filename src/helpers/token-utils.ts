export const isTokenExpired = (token: string | null) => {
  if (!token) return false;

  const payload = token.split('.')[1];
  const decodedPayload = atob(payload);
  const payloadObj = JSON.parse(decodedPayload);
  const expirationDate = payloadObj.exp;
  const currentTimestamp = Math.floor(Date.now() / 1000);
  return currentTimestamp >= expirationDate;
};
