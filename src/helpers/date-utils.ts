import dayjs from 'dayjs';

type DateRange = [Date, Date];

export function isValidDate(d: string) {
  const parsedDate = new Date(d);
  return parsedDate instanceof Date && !Number.isNaN(parsedDate);
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const isWithinRange = (row: any, columnId: string, value: DateRange) => {
  const dateString = row.getValue(columnId);
  const date = dayjs(dateString, 'M/D/YYYY').toDate();

  const [start, end] = value;

  if ((start || end) && !date) return false;

  if (start && !end) {
    return date.getTime() >= start.getTime();
  } else if (!start && end) {
    return date.getTime() <= end.getTime();
  } else if (start && end) {
    return date.getTime() >= start.getTime() && date.getTime() <= end.getTime();
  } else return true;
};
