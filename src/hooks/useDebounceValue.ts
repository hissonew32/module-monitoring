import { useState } from 'react'

export const useDebouncedValue = (initialValue: string, delay: number, callback: (value: string) => void) => {
	const [value, setValue] = useState(initialValue)
	const [typingTimeout, setTypingTimeout] = useState<number | null>(null)

	const handleChange = (newValue: string) => {
		setValue(newValue)

		if (typingTimeout !== null) {
			window.clearTimeout(typingTimeout)
		}

		setTypingTimeout(
			window.setTimeout(() => {
				callback(newValue)
			}, delay)
		)
	}

	return [value, handleChange] as const
}
