/* eslint-disable @typescript-eslint/no-explicit-any */
import DownloadIcon from "@/assets/icons/download.svg";
import MenuIcon from "@/assets/icons/menu.svg";
import { DownloadFile } from "@/services/DownloadFile";
import { useMonitoring } from "@/stores/participants/useParticipants";
import { Menu, Transition } from "@headlessui/react";
import { Fragment } from "react";

function classNames(...classes: string[]) {
  return classes.filter(Boolean).join(" ");
}

type Dropdown<T> = {
  row: T;
};

const Dropdown = <T,>({ row }: Dropdown<T | any>) => {
  const { setOverlay } = useMonitoring();
  const checkExams = row.exams && row.exams.length > 0;

  const options = [
    ...((checkExams && row.status === "2")
      ? [
          // SI ESTA APROBADO SI MUESTRA EL QR
          {
            icon: DownloadIcon,
            text: "Descargar QR",
            onClick: async () => {
              setOverlay(true);

              // SI EN CASO NO TIENE EXAMENES NO DEBERIA APARECER ESTO

              try {
                const async_asign_quotas_participants_id = row?.exams[0]?.async_asign_quotas_participants_id;

                await DownloadFile(
                  { id: async_asign_quotas_participants_id },
                  "/async/contact.provider/download.participant.qr",
                  row?.code_qr?.filename + ".jpg"
                );
              } catch (error) {
                //
              } finally {
                setOverlay(false);
              }
            },
          },
          // MUESTRA SOLO SI TIENE EXAMEN
          {
            icon: DownloadIcon,
            text: "Descargar Registro de Capacitación Firmada",
            onClick: async () => {
              setOverlay(true);

              try {
                await DownloadFile(
                  { asign_quotas_participants_id: row.id },
                  "/async/contact.supervisor/download.participant.attended",
                  "Registro de Capacitación Firmada.pdf"
                );
              } catch (error) {
                //
              } finally {
                setOverlay(false);
              }
            },
          },
        ]
      : []),
  ];

  return (
    <Menu as="div" className="relative inline-block text-left">
      <div>
        <Menu.Button className="inline-flex w-full justify-center gap-x-1.5 rounded-full bg-white px-2 py-2 text-sm font-semibold text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 hover:bg-gray-50">
          {/* -mr-1 h-5 w-5 text-gray-400 */}
          <img src={MenuIcon} className="h-5 w-5 text-gray-400" alt="" />
        </Menu.Button>
      </div>

      <Transition
        as={Fragment}
        enter="transition ease-out duration-100"
        enterFrom="transform opacity-0 scale-95"
        enterTo="transform opacity-100 scale-100"
        leave="transition ease-in duration-75"
        leaveFrom="transform opacity-100 scale-100"
        leaveTo="transform opacity-0 scale-95"
      >
        <Menu.Items className="absolute right-0 z-10 mt-2 w-max origin-top-right divide-y divide-gray-100 rounded-md bg-white shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none">
          <div className="py-1">
            {options.map((option, key) => (
              <Menu.Item key={key}>
                {({ active }) => (
                  <a
                    className={classNames(
                      active ? "bg-gray-100 text-gray-900" : "text-gray-700",
                      "flex items-center px-4 py-2 text-sm cursor-pointer"
                    )}
                    onClick={option.onClick}
                  >
                    <img className="h-5 w-5 mr-3" src={option.icon} alt="" />

                    {option.text}
                  </a>
                )}
              </Menu.Item>
            ))}
          </div>
        </Menu.Items>
      </Transition>
    </Menu>
  );
};

export default Dropdown;
