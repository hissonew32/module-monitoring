import DownloadIcon from '@/assets/icons/download.svg'
import Button from '@/components/Atoms/Button'
import Card from '@/components/Atoms/Card'
import DateInput from '@/components/Atoms/DateInput'
import SearchInput from '@/components/Atoms/SearchInput'
import TablePagination from '@/components/Atoms/TablePagination'
import { useDebouncedValue } from '@/hooks/useDebounceValue'
import { DownloadFile } from '@/services/DownloadFile'
import { useMonitoring } from '@/stores/participants/useParticipants'
import { ColumnDef, flexRender, getCoreRowModel, useReactTable } from '@tanstack/react-table'
import dayjs from 'dayjs'
import { useEffect, useState } from 'react'

type Meta = {
	align?: 'left' | 'center' | 'right'
}

type TableBase<T> = {
	data: T[]
	columns: ColumnDef<T>[]
}

const TableBase = <T,>({ data, columns }: TableBase<T>) => {
	const { filters, setFilter, setPagination, total } = useMonitoring()
	const [paginationData, setPaginationData] = useState({
		pageIndex: 0, // page
		pageSize: 7 // perPage
	})

	const table = useReactTable({
		columns,
		data,
		getCoreRowModel: getCoreRowModel(),
		state: {
			pagination: paginationData
		},
		pageCount: Math.ceil(total / paginationData.pageSize),
		onPaginationChange: setPaginationData,
		manualPagination: true
	})

	const { getHeaderGroups, getRowModel } = table

	const [companySearchValue, setCompanySearchValue] = useDebouncedValue('', 300, (value: string) => onColumnFilter('company_search', value))
	const [identifySearchValue, setIdentifySearchValue] = useDebouncedValue('', 300, (value: string) => onColumnFilter('identify_search', value))

	const onColumnFilter = (id: string, value: string) => {
		setFilter({ ...filters, [id]: value })
		setPaginationData({ ...paginationData, pageIndex: 0 })
	}

	const handleStartDateChange = (value: string) => {
		const startDate = dayjs(value).toDate()
		const endDate = filters.end_date ? dayjs(filters.end_date).toDate() : null

		if (value === '') {
			setFilter({
				...filters,
				start_date: '',
				end_date: ''
			})
			return
		}

		if (!endDate) {
			setFilter({
				...filters,
				start_date: value
			})
			return
		}

		if (endDate && startDate.getTime() > endDate.getTime()) {
			setFilter({
				...filters,
				start_date: value,
				end_date: value
			})
		} else {
			setFilter({
				...filters,
				start_date: value
			})
		}

		setPaginationData({ ...paginationData, pageIndex: 0 })
	}

	const handleEndDateChange = (value: string) => {
		if (value === '') {
			setFilter({
				...filters,
				end_date: ''
			})
			return
		}

		setFilter({
			...filters,
			end_date: value
		})
		setPaginationData({ ...paginationData, pageIndex: 0 })
	}

	const onDownloadNotes = async () => {
		await DownloadFile({...filters}, 'async/contact.supervisor/download.participant.notes', 'Notas de Participantes.xlsx')
	}

	useEffect(() => {
		setPagination({ page: table.getState().pagination.pageIndex + 1, perPage: table.getState().pagination.pageSize })
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [paginationData])

	return (
		<>
			<Card>
				<div className='grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-5 gap-2'>
					<SearchInput
						value={companySearchValue}
						placeholder='Filtrar por empresa'
						onChange={(e) => setCompanySearchValue(e.target.value)}
						type='search'
					/>
					<SearchInput
						value={identifySearchValue}
						placeholder='Filtrar por dni'
						onChange={(e) => setIdentifySearchValue(e.target.value)}
						type='search'
					/>

					<DateInput
						value={filters.start_date ? dayjs(filters.start_date).format('YYYY-MM-DD') : ''}
						onChange={(e) => handleStartDateChange(e.target.value)}
					/>
					<DateInput
						value={filters.end_date ? dayjs(filters.end_date).format('YYYY-MM-DD') : ''}
						onChange={(e) => handleEndDateChange(e.target.value)}
						min={dayjs(filters.start_date || '').format('YYYY-MM-DD')}
						disabled={!filters.start_date}
					/>

					<Button onClick={onDownloadNotes} icon={DownloadIcon} text='Descargar' />
				</div>
			</Card>

			<Card className='mt-4 mb-4 max-w-full overflow-x-auto'>
				<table className='w-full table-auto text-sm text-left'>
					{/* HEADER */}
					<thead className='text-gray-600 font-medium border-b'>
						{getHeaderGroups().map((headerGroup) => (
							<tr key={headerGroup.id}>
								{headerGroup.headers.map((header) => {
									return (
										<th
											key={header.id}
											colSpan={header.colSpan}
											align={(header.column.columnDef.meta as Meta)?.align}
											className='py-3 pr-6'
											style={{ width: header.getSize() }}
										>
											{header.isPlaceholder ? null : (
												<>
													<div
														{
															...{
																// className: header.column.getCanSort() ? 'cursor-pointer select-none' : ''
																// onClick: header.column.getToggleSortingHandler()
															}
														}
													>
														{flexRender(header.column.columnDef.header, header.getContext())}
														{/* {{
															asc: ' 🔼',
															desc: ' 🔽'
														}[header.column.getIsSorted() as string] ?? null} */}
													</div>
												</>
											)}
										</th>
									)
								})}
							</tr>
						))}
					</thead>
					{/* END HEADER */}
					<tbody className='text-gray-600 divide-y'>
						{getRowModel().rows.length ? (
							getRowModel().rows.map((row) => {
								return (
									<tr key={row.id}>
										{row.getVisibleCells().map((cell) => {
											return (
												<td
													key={cell.id}
													align={(cell.column.columnDef.meta as Meta)?.align}
													className='pr-6 py-4 whitespace-nowrap'
												>
													{flexRender(cell.column.columnDef.cell, cell.getContext())}
												</td>
											)
										})}
									</tr>
								)
							})
						) : (
							<tr className='text-center h-32'>
								<td colSpan={12}>No se encontraron registros.</td>
							</tr>
						)}
					</tbody>
				</table>

				<TablePagination table={table} />
			</Card>
		</>
	)
}

export default TableBase
