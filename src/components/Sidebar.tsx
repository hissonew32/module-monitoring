import PromotIcono from '@/assets/Promot-icono.svg';
import PowerIcon from '@/assets/icons/power.svg';
import { useAuth } from '@/stores/Auth';
import { useSidebar } from '@/stores/Sidebar';
import { useEffect, useRef } from 'react';
import { useNavigate } from 'react-router-dom';

const Sidebar = () => {
  const { logout } = useAuth();
  const { isOpened, toggle } = useSidebar();
  const sidebarRef = useRef<HTMLDivElement>(null);
  const navigate = useNavigate()

  useEffect(() => {
    const handleClickOutside = (event: MouseEvent) => {
      if (
        sidebarRef.current &&
        !sidebarRef.current.contains(event.target as Node) &&
        isOpened
      ) {
        toggle();
      }
    };

    const handleResize = () => {
      if (window.innerWidth >= 1024 && isOpened) toggle();
    };

    document.addEventListener('mousedown', handleClickOutside);
    window.addEventListener('resize', handleResize);

    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
      window.removeEventListener('resize', handleResize);
    };
  }, [isOpened, toggle]);

  const handleLogout = async () => {
    await logout();
    navigate('/')
  };

  return (
    <div
      ref={sidebarRef}
      className={`${
        isOpened ? 'mr-0 lg:mr-60 block' : 'mr-60 hidden lg:block'
      } `}
    >
      <div className='fixed z-50'>
        <div className='flex h-screen flex-col justify-between border-e bg-white w-60'>
          <div className='px-4 py-6'>
            <div className='flex items-center px-4'>
              <div className=''>
                <img
                  className='w-8 h-8'
                  height={80}
                  width={80}
                  src={PromotIcono}
                  alt='logo'
                />
              </div>

              <div className='flex-1 mx-4'>
                <span className='text-gray-600 font-bold'>PROMOT</span>
              </div>
            </div>

            <ul className='mt-6 space-y-1'>
              <li>
                <a
                  href='#'
                  className='block rounded-lg bg-gray-100 px-4 py-2 text-sm font-medium text-gray-700'
                >
                  General
                </a>
              </li>
            </ul>
          </div>

          <div className='sticky inset-x-0 bottom-0 border-t border-gray-100'>
            <a
              onClick={handleLogout}
              className='flex items-center gap-2 bg-white p-4 hover:bg-gray-50 cursor-pointer'
            >
              <img src={PowerIcon} alt='' className='size-6' />
              Cerrar sesión
            </a>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Sidebar;
