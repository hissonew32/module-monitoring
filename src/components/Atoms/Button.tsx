import { ButtonHTMLAttributes } from 'react';

type Button = {
  icon?: string;
  text?: string;
} & ButtonHTMLAttributes<HTMLButtonElement>;

const Button = ({ icon, text, ...buttonProps }: Button) => {
  return (
    <button
      className='flex items-center justify-center gap-2 px-5 py-2 text-gray-700 duration-100 border rounded-lg hover:border-indigo-600 active:shadow-lg'
      {...buttonProps}
    >
      {icon && <img src={icon} className='h-5 w-5' alt='' />}

      {text}
    </button>
  );
};

export default Button;
