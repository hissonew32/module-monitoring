type Card = {
  children: React.ReactNode;
  className?: string;
};

const Card = ({ children, className = '' }: Card) => {
  return (
    <div className={'border rounded-lg p-4 shadow ' + className}>
      {children}
    </div>
  );
};

export default Card;
