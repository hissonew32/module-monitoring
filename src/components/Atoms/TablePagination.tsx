type TablePagination<T> = {
  table: import('@tanstack/table-core').Table<T>;
};

const TablePagination = <T,>({ table }: TablePagination<T>) => {
  return (
    <div className='max-w-screen-xl mx-auto mt-4 px-4 text-gray-600 md:px-8'>
      <div
        className='hidden items-center justify-between sm:flex'
        aria-label='Pagination'
      >
        {/* first page */}
        <button
          className='hover:bg-gray-100 px-4 py-2 rounded flex items-center gap-x-2 cursor-pointer select-none hover:text-indigo-600'
          onClick={() => table.setPageIndex(0)}
        >
          <svg
            className='w-5 h-5'
            fill='none'
            strokeWidth={1.5}
            stroke='currentColor'
            viewBox='0 0 24 24'
            xmlns='http://www.w3.org/2000/svg'
            aria-hidden='true'
          >
            <path
              strokeLinecap='round'
              strokeLinejoin='round'
              d='m18.75 4.5-7.5 7.5 7.5 7.5m-6-15L5.25 12l7.5 7.5'
            />
          </svg>
        </button>

        {/* previous page */}
        <button
          className='hover:bg-gray-100 px-4 py-2 rounded disabled:opacity-30 disabled:cursor-not-allowed hover:text-indigo-600 flex items-center gap-x-2 cursor-pointer select-none'
          disabled={!table.getCanPreviousPage()}
          onClick={() => table.previousPage()}
        >
          <svg
            className='w-5 h-5'
            fill='none'
            strokeWidth={1.5}
            stroke='currentColor'
            viewBox='0 0 24 24'
            xmlns='http://www.w3.org/2000/svg'
            aria-hidden='true'
          >
            <path
              strokeLinecap='round'
              strokeLinejoin='round'
              d='M15.75 19.5 8.25 12l7.5-7.5'
            />
          </svg>
        </button>

        <ul className='flex items-center gap-1 select-none'>
          Pagina {table.getState().pagination.pageIndex + 1} de{' '}
          {table.getPageCount()}
        </ul>

        {/* next page */}
        <button
          className='hover:bg-gray-100 px-4 py-2 rounded disabled:opacity-30 disabled:cursor-not-allowed hover:text-indigo-600 flex items-center gap-x-2 cursor-pointer select-none'
          disabled={!table.getCanNextPage()}
          onClick={() => table.nextPage()}
        >
          <svg
            className='h-5 w-5'
            fill='none'
            strokeWidth={1.5}
            stroke='currentColor'
            viewBox='0 0 24 24'
            xmlns='http://www.w3.org/2000/svg'
            aria-hidden='true'
          >
            <path
              strokeLinecap='round'
              strokeLinejoin='round'
              d='m8.25 4.5 7.5 7.5-7.5 7.5'
            />
          </svg>
        </button>

        {/* last page */}
        <button
          className='hover:bg-gray-100 px-4 py-2 rounded hover:text-indigo-600 flex items-center gap-x-2 cursor-pointer select-none'
          onClick={() => table.setPageIndex(table.getPageCount() - 1)}
        >
          <svg
            className='w-5 h-5'
            fill='none'
            strokeWidth={1.5}
            stroke='currentColor'
            viewBox='0 0 24 24'
            xmlns='http://www.w3.org/2000/svg'
            aria-hidden='true'
          >
            <path
              strokeLinecap='round'
              strokeLinejoin='round'
              d='m5.25 4.5 7.5 7.5-7.5 7.5m6-15 7.5 7.5-7.5 7.5'
            />
          </svg>
        </button>
      </div>
      {/* On mobile version */}
      <div className='flex items-center justify-between text-sm text-gray-600 font-medium sm:hidden'>
        <a
          onClick={() => table.nextPage()}
          className='px-4 py-2 border rounded-lg duration-150 hover:bg-gray-50 cursor-pointer'
        >
          Anterior
        </a>
        <div className='font-medium'>
          {/* Page {currentPage} of {pages.length} */}
          Page {table.getState().pagination.pageIndex + 1} of{' '}
          {table.getPageCount()}
        </div>
        <a
          onClick={() => table.nextPage()}
          className='px-4 py-2 border rounded-lg duration-150 hover:bg-gray-50 cursor-pointer'
        >
          Siguiente
        </a>
      </div>
    </div>
  );
};

export default TablePagination;
