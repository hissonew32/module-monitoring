import { InputHTMLAttributes } from 'react';

const DateInput = (props: InputHTMLAttributes<HTMLInputElement>) => {
  return (
    <input
      type='date'
      className='w-full px-3 py-2 text-gray-500 bg-transparent outline-none border focus:border-indigo-600 rounded-lg'
      {...props}
    />
  );
};

export default DateInput;
