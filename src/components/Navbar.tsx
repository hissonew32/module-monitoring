import Profile from '@/assets/EmptyProfile.svg'
import MenuIcon from '@/assets/icons/menu.svg'
import Button from '@/components/Atoms/Button'

import { useAuth } from '@/stores/Auth'
import { useSidebar } from '@/stores/Sidebar'

const Navbar = () => {
	const { user } = useAuth()
	const { toggle } = useSidebar()

	return (
		<div className='border rounded-lg m-4 shadow' dir='rtl'>
			<div className='flex items-center justify-between p-4 px-6'>
				<a href='#' className='flex items-center gap-4 bg-white hover:bg-gray-50 px-4'>
					<img
						alt='profile image'
						src={Profile}
						width={40}
						height={40}
						className='rounded-full border-1 bg-slate-300 p-2'
					/>

					<p className='text-md leading-4'>
						<strong className='block font-medium'>
							{user?.name} {user?.surname}
						</strong>

						<span className='text-xs'> {user?.email} </span>
					</p>
				</a>
				<Button icon={MenuIcon} text='' className='block' onClick={() => toggle()} />
			</div>
		</div>
	)
}

export default Navbar
