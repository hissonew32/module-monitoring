/* eslint-disable @typescript-eslint/no-explicit-any */
import Dropdown from '@/components/Organism/Dropdown'
import TableBase from '@/components/Organism/TableBase'
// import { isWithinRange } from '@/helpers/date-utils'
import { useMonitoring } from '@/stores/participants/useParticipants'
import type { ColumnDef } from '@tanstack/react-table'
import dayjs from 'dayjs'
import { useMemo } from 'react'

function toGetColor(status: '2' | '1' | '3') {
	const baseClassName = 'py-1 px-3 rounded-full font-semibold text-xs inline '
	const labelColors = {
		'2': {
			color: 'text-green-600 bg-green-50'
		},
		'1': {
			color: 'text-blue-600 bg-blue-50'
		},
		'3': {
			color: 'text-red-600 bg-red-50'
		}
	}

	return baseClassName + labelColors[status]?.color
}

const ListMonitoring = () => {
	const { participants } = useMonitoring()
	const data = useMemo(() => participants, [participants])

	const columns = useMemo<ColumnDef<any>[]>(
		() => [
			{
				header: 'Razon social',
				accessorKey: 'company_search',
				size: 150
			},
			{
				header: 'DNI',
				accessorKey: 'identify_search',
				size: 100
			},
			{
				header: 'Participante',
				accessorKey: 'full_name',
				size: 150
			},
			{
				header: 'Curso',
				accessorKey: 'course',
				size: 150
			},
			{
				header: 'Nota',
				accessorKey: 'note',
				meta: {
					align: 'center'
				},
				size: 50
			},
			{
				header: 'Fecha examen',
				accessorKey: 'exam_date',
				cell: (props) => {
					const { exam_date } = props.row.original
					return exam_date ? dayjs(exam_date).format('DD-MM-YYYY') : '~'
				},
				meta: {
					align: 'center'
				},
				size: 100
			},
			{
				header: 'Fecha de sustitutorio',
				accessorKey: 'replacement_date',
				cell: (props) => {
					const { replacement_date } = props.row.original
					return replacement_date ? dayjs(replacement_date).format('DD-MM-YYYY') : '~'
				},
				meta: {
					align: 'center'
				},
				size: 100
			},
			{
				header: 'Estado',
				cell: (props) => (
					<div className={toGetColor(props.row.original.status)}>
						{props.row.original.status == '1'
							? 'Pendiente'
							: props.row.original.status == '2'
							? 'Aprobado'
							: props.row.original.status == '3'
							? 'Desaprobado'
							: ''}
					</div>
				),
				meta: {
					align: 'center'
				},
				size: 100
			},
			{
				header: 'Acciones',
				meta: {
					align: 'center'
				},
				cell: (props) => <Dropdown row={props.row.original} />,
				size: 100
			}
		],
		[]
	)

	return <TableBase columns={columns} data={data} />
}

export default ListMonitoring
