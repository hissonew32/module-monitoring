type Container = {
  children: React.ReactNode;
};

const Container = ({ children }: Container) => {
  return <div className='mx-4'>{children}</div>;
};

export default Container;
