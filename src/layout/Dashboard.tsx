import { isTokenExpired } from '@/helpers/token-utils'
import { useAuth } from '@/stores/Auth'
import { useEffect } from 'react'
import { Navigate } from 'react-router-dom'
import { Toaster } from 'sonner'
import Container from '../components/Container'
import Navbar from '../components/Navbar'
import Sidebar from '../components/Sidebar'

type Dashboard = {
	children: React.ReactNode
}

function Dashboard({ children }: Dashboard) {
	const { token } = useAuth()

	useEffect(() => {
		if (token) {
			const expired = isTokenExpired(token);
			if (expired) {
				localStorage.clear();
				window.location.reload();
			}
		}
	}, [token]);

	return (
		<div className='flex'>
			{token ? (
				<>
					<Sidebar />
					<div className='flex-1' style={{ width: 'calc(100dvw - 272px)' }}>
						<Navbar />
						<Container>{children}</Container>
					</div>
					<Toaster richColors position='top-center' />
				</>
			) : (
				<Navigate to='/' replace />
			)}
		</div>
	)
}

export default Dashboard
