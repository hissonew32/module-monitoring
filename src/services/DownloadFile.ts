/* eslint-disable @typescript-eslint/no-explicit-any */
import axiosClient from '@/axiosClient'

export async function DownloadFile(body: any, url: string, filename?: string) {
	const { data } = await axiosClient.post<any>(url, body, { responseType: 'blob' })

	const a = document.createElement('a')
	a.href = URL.createObjectURL(data)
	a.download = filename || 'file'
	document.body.appendChild(a)
	a.click()
	document.body.removeChild(a)
}
