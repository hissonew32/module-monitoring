/* eslint-disable @typescript-eslint/no-explicit-any */
import axiosClient from '@/axiosClient'

export type Participant = {
	razon_social: string
	participant: string
	category_name: string
	course: string
	note: number
	dni: string
	exam_dates: string
	replacement_date: string
	status: 'Aprobado' | 'Pendiente' | 'Desaprobado'
}

export async function getParticipants(params: any) {
	const { data, status } = await axiosClient.get<any>('/async/contact.supervisor/list', { params })

	return status === 200 ? data : null
}
