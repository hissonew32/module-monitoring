import axiosClient from '@/axiosClient';

export type Credentials = {
  email: string;
  password: string;
  host?: string;
};

export type User = {
  // id: number;
  // full_name: string;
  // identify_type: string;
  // identify_value: number;
  email: string;
  // job: string;
  // category: string;
  name: string;
  surname: string;
  role: string;
};

export async function AuthLogin(credentials: Credentials) {
  const { data } = await axiosClient.post('/login', credentials);

  return data;
  // return status === 200 ? data : null;
}
