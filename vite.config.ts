import react from "@vitejs/plugin-react-swc";
import { defineConfig, loadEnv } from "vite";

const defaultOutDir = "dist"

// https://vitejs.dev/config/
export default defineConfig(({ mode }) => {
  const env = loadEnv(mode, process.cwd(), '')

  return {
    plugins: [react()],
    build: {
      chunkSizeWarningLimit: 8192,
      outDir: env.VITE_TYPE ? ("dist_" + env.VITE_TYPE) : defaultOutDir,
    },
    resolve: {
      alias: {
        "@": "/src",
        "~": "/public",
      },
    },
  };
});
